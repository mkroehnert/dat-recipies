// SPDX-License-Identifier: MPL-2.0
/*
  Copyright (C) 2018-2019 The Dat Recipies Authors.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

'use strict'

export class DatRecipieDb {
  constructor (datArchive, recipieDirectory) {
    this.datArchive = datArchive
    this.recipieDirectory = recipieDirectory
    this.recipies = []
    this.JSON_SPACE = '  '
  }

  getRecipiePath (filename) {
    return this.recipieDirectory + filename
  }

  async getRecipies () {
    let self = this
    let files = await self.datArchive.readdir(self.recipieDirectory)
    this.recipies = await Promise.all(
      files
        .filter(file => file.endsWith('.json'))
        .map(file => {
          return self.datArchive.readFile(self.getRecipiePath(file))
            .then(content => {
              // return a dictionary mapping filename to the parsed JSON content
              return {
                'filename': file,
                'json': JSON.parse(content)
              }
            })
        }))
    return this.recipies
  }

  getRecipie (recipieId) {
    let recipie = this.recipies.filter(recipie => recipie.filename === recipieId)
    if (!recipie || recipie.length <= 0) {
      console.log('missing recipie: ' + recipieId)
      return null
    }
    return recipie[0]
  }

  deleteRecipie (recipieFilename) {
    console.log('DB: deleting recipie: ' + recipieFilename)
    return this.datArchive
      .unlink(this.getRecipiePath(recipieFilename))
  }

  updateImageOfRecipie (recipieFilename, imageDataUri) {
    let recipie = this.getRecipie(recipieFilename)
    if (recipie === null) {
      return
    }

    if (recipie.json.image === imageDataUri) {
      return
    }

    recipie.json.image = imageDataUri

    let filename = this.getRecipiePath(recipie.filename)

    this.datArchive
      .writeFile(filename, JSON.stringify(recipie.json, null, this.JSON_SPACE), 'utf8')
      .then(ev => console.log('DB: file updated: ' + ev))
      .catch(ex => console.log('DB: file update exception: ' + ex))
      // await this.datArchive.commit()
  }
}
