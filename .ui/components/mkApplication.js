// SPDX-License-Identifier: MPL-2.0
/*
  Copyright (C) 2019 The Dat Recipies Authors.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

/* globals DOMParser, HTMLElement, DatArchive */

'use strict'

import { mkApplicationHtmlTemplate } from './mkApplication.htmlTemplate.js'
import { mkApplicationCss } from './mkApplication.css.js'

import { DatRecipieDb } from '../db.js'

let appHtmlTemplate = new DOMParser()
  .parseFromString(mkApplicationHtmlTemplate, 'text/html')
  .querySelector('template')

let appStyle = document.createElement('style')
appStyle.textContent = mkApplicationCss

function addTemplateToShadowDom (shadowRoot) {
  // append CSS first
  shadowRoot.appendChild(appStyle.cloneNode(true))

  // append HTML second
  shadowRoot.appendChild(appHtmlTemplate.content.cloneNode(true))
}

const applicationNameAttr = 'application-name'
const recipiesDir = '/recipies/'

customElements.define('mk-application', class extends HTMLElement {
  constructor () {
    super()
  }

  connectedCallback () {
    const shadowRoot = this.attachShadow({ mode: 'open' })
    addTemplateToShadowDom(shadowRoot)

    // You can also put checks to see if attr is present or not
    // and throw errors to make some attributes mandatory
    // Also default values for these variables can be defined here
    this.applicationName = this.getAttribute(applicationNameAttr)

    this.recipieList = document.querySelector('mk-recipie-list')
    this.recipieDetails = document.querySelector('mk-recipie-details')

    this.setupApplication()
  }

  async setupApplication () {
    if (beaker && beaker.hyperdrive) {
      const hyperUrl = this.hyperUrlFromLocation(window.location.pathname)
      document.getElementById('url').innerText = hyperUrl
      document.getElementById('url').href = hyperUrl
      document.getElementById('library-url').href = hyperUrl

      this.archive = await beaker.hyperdrive.drive(hyperUrl)
      this.recipiesDb = new DatRecipieDb(this.archive, recipiesDir)
      this.recipieList.recipieList = await this.recipiesDb.getRecipies()
      this.setupEventListeners()
    } else {
      document.getElementById('url').innerText = 'Dat not supported by this browser'
    }
  }

  hyperUrlFromLocation(location) {
    const regexResult = RegExp('^/(?<hyperHash>[0-9,a-z]{64})/?$', 'i').exec(location)
    if (regexResult != null) {
      return 'hyper://' + regexResult.groups.hyperHash
    } else {
      return location.origin
    }
  }

  setupEventListeners () {
    this.recipieList.addEventListener('RecipieClicked', event => {
      this.recipieList.hide()
      let recipieDict = this.recipiesDb.getRecipie(event.detail.recipieId)
      this.recipieDetails.show(recipieDict)
    })
    this.recipieList.addEventListener('RecipieDeleted', event => {
      this.recipiesDb.deleteRecipie(event.detail.recipieId)
    })
    this.recipieDetails.addEventListener('RecipieClosed', () => {
      this.recipieList.show()
      this.recipieDetails.hide()
    })
    this.recipieDetails.addEventListener('RecipieImageUpdated', event => {
      let detail = event.detail
      console.log(detail)
      this.recipiesDb.updateImageOfRecipie(detail.recipieId, detail.imageUri)
    })
  }

  // Getter to let component know what attributes
  // to watch for mutation
  static get observedAttributes () {
    return [applicationNameAttr]
  }

  attributeChangedCallback (attr, oldValue, newValue) {
    const attribute = attr.toLowerCase()
    if (attribute === applicationNameAttr) {
      this.applicationName = newValue !== '' ? newValue : 'Not Provided!'
    }
  }

  // Define setters to update the DOM whenever these values are set
  set applicationName (value) {
    this._applicationName = value
    if (this.shadowRoot) {
      // this.shadowRoot.querySelector('#applicationName').textContent = value
    }
  }

  get applicationName () {
    return this._applicationName
  }
})
