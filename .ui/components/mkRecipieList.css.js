// SPDX-License-Identifier: MPL-2.0
/*
  Copyright (C) 2019 The Dat Recipies Authors.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

'use strict'

const headerCss = `
#recipie-selection-header {
  display: flex;
  flex-direction: row;
}

.spacer {
  flex: 1;
}

#recipie-selection-delete {
    display: inline-block;
    overflow: hidden;
    border: none;
    outline: 0;
    text-align: center;
    vertical-align: middle;
    text-decoration: none;
    font: inherit;
    background-color: var(--recipies-delete-color);
}

#recipie-selection-title {
  text-align: center;
}

#recipie-selection-search-button {
    display: inline-block;
    overflow: hidden;
    border: none;
    outline: 0;
    font: inherit;
    background-color: var(--recipies-background-color);
    width: 0;
    padding-right: 5%;
}
`

const searchHeaderCss = `
#recipie-selection-header-search {
  display: flex;
  flex-direction: row;
}

#recipie-selection-search-title{
  text-align: left;
  padding-left: 5px;
}

#recipie-selection-search-input-container {
    display: inline-block;
    margin: 0 10px;
    padding: 0;
    outline: 0;
    width: 100%;
    font-size: 100%;
}

#recipie-selection-search-input {
    display: inline-block;
    box-sizing: border-box;
    border: 0;
    padding: 5px;
    outline: 0;
    width: 100%;
    height: 100%;
    font-size: 100%;
}

#recipie-selection-search-close-button {
    display: block;
    overflow: hidden;
    border: none;
    outline: 0;
    width: 0;
    padding-right: 5%;
}
`

const style = document.createElement('style')
style.textContent = `
:host {
  display: block;
  contain: content;
}

#recipie-selection-container {
}

${headerCss}

${searchHeaderCss}

.search {
    background: url(/.ui/assets/search.svg);
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center center;
}

.close {
    background: url(/.ui/assets/close.svg);
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center center;
}

.selected {
    background-color: var(--recipies-delete-color) !important;
}

#recipie-selection-list {
  list-style-type: none;
  margin: 0;
  padding: 0;
}

#recipie-selection-list li {
  padding: 10px;
  padding-left: 1em;
}

#recipie-selection-list li:hover {
  background: var(--recipies-hover-color);
}
`

export function css () {
  return style.cloneNode(true)
}
