// SPDX-License-Identifier: MPL-2.0
/*
  Copyright (C) 2019 The Dat Recipies Authors.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

'use strict'

let style = document.createElement('style')
style.textContent = `
:host {
    display: block;
    contain: content;
}

#recipie-details-container {
}

#recipie-header {
  display: flex;
  flex-direction: row;
}

.spacer {
  flex: 1;
}

#recipie-header-back {
    overflow: hidden;
    border: none;
    outline: 0;
    text-align: center;
    vertical-align: middle;
    text-decoration: none;
    font: inherit;
    background-color: var(--recipies-hover-color);
}

#recipie-header-name {
    vertical-align: middle;
    text-align: center;
}

#recipie-header-download {
    display: inline-block;
    overflow: hidden;
    border: none;
    outline: 0;
    text-align: center;
    vertical-align: middle;
    text-decoration: none;
    font: inherit;
    background-color: var(--recipies-hover-color);
}

mk-image-drop {
    height: 300px;
}

#recipie-details-content {
  padding: 0.5em;
}

#recipie-details-content h3 {
  margin: 28px 0 10px;
}

#recipie-ingredients {
  list-style: square;
}

#recipie-preparation {
}
`

export function css () {
  return style.cloneNode(true)
}
