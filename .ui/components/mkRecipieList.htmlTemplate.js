// SPDX-License-Identifier: MPL-2.0
/*
  Copyright (C) 2019 The Dat Recipies Authors.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

'use strict'

const header = `
      <div id="recipie-selection-header" class="content-grid-header">
        <!-- delete -->
        <button id="recipie-selection-delete" class="hide">Delete</button>
        <!-- caption -->
        <span class="spacer"></span>
        <span id="recipie-selection-title">Recipies</span>
        <span class="spacer"></span>
        <!-- toggle search -->
        <button id="recipie-selection-search-button" class="search"></button>
      </div>
`
const searchHeader = `
      <div id="recipie-selection-header-search" class="content-grid-header hide">
        <div id="recipie-selection-search-title">Search</div>
        <!-- search box -->
        <span class="spacer"></span>
        <span id="recipie-selection-search-input-container">
          <input id="recipie-selection-search-input" type="text" placeholder="Enter Recipie Name"></input>
        </span>
        <span class="spacer"></span>
        <!-- close search -->
        <button id="recipie-selection-search-close-button" class="close"></button>
      </div>
`

const htmlTemplateString = `
  <template id="mk-recipie-list-template">
    <div id ="recipie-selection-container" class="content-grid">

      ${header}

      ${searchHeader}

      <ul id="recipie-selection-list" class="content-grid-content"></ul>

    </div>
  </template>
`

const htmlTemplateNode = new DOMParser()
  .parseFromString(htmlTemplateString, 'text/html')
  .querySelector('template')

export function htmlTemplate () {
  return htmlTemplateNode.content.cloneNode(true)
}
