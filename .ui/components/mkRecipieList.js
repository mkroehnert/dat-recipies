// SPDX-License-Identifier: MPL-2.0
/*
  Copyright (C) 2019 The Dat Recipies Authors.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

/* globals customElements, HTMLElement, HTMLLIElement, CustomEvent */

'use strict'

import { htmlTemplate } from './mkRecipieList.htmlTemplate.js'
import { css as sharedCss } from './mkRecipies.css.js'
import { css } from './mkRecipieList.css.js'

// uses flex layout for header from  https://hacks.mozilla.org/2018/02/css-grid-for-ui-layouts/

// external events

function emitRecipieClicked (self, recipieId) {
  let event = new CustomEvent('RecipieClicked', {
    detail: {
      recipieId: recipieId
    },
    bubbles: true,
    composed: true // bubble outside of shadowRoot
  })
  self.dispatchEvent(event)
}

function emitRecipieDeleted (self, recipieId) {
  let event = new CustomEvent('RecipieDeleted', {
    detail: {
      recipieId: recipieId
    },
    bubbles: true,
    composed: true // bubble outside of shadowRoot
  })
  self.dispatchEvent(event)
}

// internal events

function emitRecipieSelected (self) {
  let event = new CustomEvent('RecipieSelected', {
    detail: {},
    bubbles: true
  })
  self.dispatchEvent(event)
}

function emitRecipieSelectionCleared (self) {
  let event = new CustomEvent('RecipieSelectionCleared', {
    detail: {},
    bubbles: true
  })
  self.dispatchEvent(event)
}

// document.activeElement.shadowRoot.activeElement // get element focused in shadowDom

// usage
// <li is="mk-li"></li>
// document.createElement('li', {is: 'mk-li'})
customElements.define('mk-li', class extends HTMLLIElement {
  constructor () {
    super()
    this.timer = null
  }

  connectedCallback () {
    this.addEventListener('click', ev => { alert('custom element clicked') })
  }
}, { extends: 'li' })

// TODO extract into MkList
class SelectionList {
  constructor (newParent) {
    this.selectionTimer = null
    this.parent = newParent
    this.parent.addEventListener('RecipieSelectionCleared', this.clearSelection.bind(this))
  }

  selectedElements () {
    return Array.from(this.elementList.getElementsByClassName('selected'))
  }

  set elementList (newList) {
    this._elementList = newList
  }
  get elementList () {
    return this._elementList
  }

  removeAllChildren () {
    let node = this.elementList
    while (node.lastChild) {
      node.removeChild(node.lastChild)
    }
  }

  appendChild (childnode) {
    this.elementList.appendChild(childnode)
  }

  // rendering
  setListElements (listElements) {
    this.removeAllChildren()
    let fragment = document.createDocumentFragment()
    listElements.forEach(li => {
      li.addEventListener('click', ev => {
        this.cancelSelection(li)
        if (li.classList.contains('longpressed')) {
          li.classList.remove('longpressed')
          return
        }
        emitRecipieSelectionCleared(li)
        let recipieId = li.id
        emitRecipieClicked(li, recipieId)
      })
      li.addEventListener('mousedown', this.startSelection(li))
      li.addEventListener('touchstart', this.startSelection(li))
      li.addEventListener('mouseup', this.cancelSelection(li))
      li.addEventListener('mouseout', this.cancelSelection(li))
      li.addEventListener('touchend', this.cancelSelection(li))
      li.addEventListener('touchleave', this.cancelSelection(li))
      li.addEventListener('touchcancel', this.cancelSelection(li))
      fragment.appendChild(li)
    })

    this.appendChild(fragment)
  }

  // selection
  startSelection (element) {
    return event => {
      if (event.type === 'click' && event.button !== 0) {
        return
      }
      if (this.selectionTimer === null) {
        this.selectionTimer = setTimeout(() => {
          event.stopPropagation()
          element.classList.toggle('selected')
          element.classList.add('longpressed')
          this.selectionTimer = null
          if (this.selectedElements().length > 0) {
            emitRecipieSelected(element)
          } else {
            emitRecipieSelectionCleared(element)
          }
        }, 1000)
      }
    }
  }

  cancelSelection (element) {
    return event => {
      if (this.selectionTimer !== null) {
        element.classList.remove('selected')
        element.classList.remove('longpressed')
        clearTimeout(this.selectionTimer)
        this.selectionTimer = null
        emitRecipieSelectionCleared(element)
      }
    }
  }

  clearSelection () {
    this.selectedElements()
      .forEach(child => {
        child.classList.remove('selected')
      })
  }

  // delete selected
  deleteSelectedRecipies () {
    this.selectedElements()
      .forEach(childElement => {
        // emit delete event so it can be removed from DB
        let recipieId = childElement.id
        emitRecipieDeleted(childElement, recipieId)
        // delete child element from list
        childElement.parentNode.removeChild(childElement)
      })
    emitRecipieSelectionCleared(this.parent)
  }
}

//
customElements.define('mk-recipie-list', class extends HTMLElement {
  constructor () {
    super()
  }

  connectedCallback () {
    const shadowRoot = this.attachShadow({ mode: 'open' })
    // append CSS first
    shadowRoot.appendChild(sharedCss())
    shadowRoot.appendChild(css())

    // append HTML second
    shadowRoot.appendChild(htmlTemplate())

    this.getElements()

    this.setupEventListeners()
  }

  getElements () {
    this.container = this.shadowRoot.getElementById('recipie-selection-container')
    this.header = this.shadowRoot.getElementById('recipie-selection-header')

    this.deleteButton = this.shadowRoot.getElementById('recipie-selection-delete')

    this.headerSearch = this.shadowRoot.getElementById('recipie-selection-header-search')

    this.searchButton = this.shadowRoot.getElementById('recipie-selection-search-button')
    this.searchInput = this.shadowRoot.getElementById('recipie-selection-search-input')
    this.searchCloseButton = this.shadowRoot.getElementById('recipie-selection-search-close-button')

    this.selectionList = new SelectionList(this.container)
    this.selectionList.elementList = this.shadowRoot.getElementById('recipie-selection-list')
  }

  setupEventListeners () {
    this.container.addEventListener('RecipieSelected', this.showDeleteButton.bind(this))
    this.container.addEventListener('RecipieSelectionCleared', this.hideDeleteButton.bind(this))
    // trigger this separately or from within deleteSelectedRecipies()?
    // this.deleteButton.addEventListener('click', emitRecipieDeleted(this))
    // this.addEventListener('DeleteRecipie', this.selectionList.deleteSelectedRecipies)
    this.deleteButton.addEventListener('click', this.selectionList.deleteSelectedRecipies.bind(this))

    this.searchButton.addEventListener('click', this.showSearchBox.bind(this))
    this.searchCloseButton.addEventListener('click', this.closeSearchBox.bind(this))
    this.searchInput.addEventListener('keyup', this.keyUp.bind(this))
  }

  renderRecipiesList () {
    this.selectionList.setListElements(this.recipieList.map(recipieDict => {
      let li = document.createElement('li')
      li.textContent = recipieDict.json.name
      li.id = recipieDict.filename
      return li
    }))
  }

  show () {
    this.container.classList.remove('hide')
  }

  hide () {
    this.container.classList.add('hide')
  }

  showDeleteButton () {
    this.deleteButton.classList.remove('hide')
  }

  hideDeleteButton () {
    this.deleteButton.classList.add('hide')
  }

  ///
  /// search
  ///
  showSearchBox () {
    this.header.classList.add('hide')
    this.headerSearch.classList.remove('hide')
    // clear delete selection
    emitRecipieSelectionCleared(this.container)
    // focus input element
    this.searchInput.focus()
  }

  closeSearchBox () {
    this.headerSearch.classList.add('hide')
    this.header.classList.remove('hide')
    this.clearSearch()
  }

  clearSearch () {
    // clear input field
    this.searchInput.value = ''
    // redisplay all elements
    this.filterRecipieList()
  }

  keyUp (event) {
    if (event.key === 'Escape') {
      this.closeSearchBox()
      return
    }
    this.filterRecipieList()
  }

  filterRecipieList () {
    let filterQuery = this.searchInput.value.toUpperCase()
    let recipieTags = this.selectionList.elementList.getElementsByTagName('li')

    // hide elements whose title doesn't match the search query
    for (var recipieTag of recipieTags) {
      let recipieName = recipieTag.innerText.toUpperCase()
      if (recipieName.indexOf(filterQuery) > -1) {
        recipieTag.classList.remove('hide')
      } else {
        recipieTag.classList.add('hide')
      }
    }
  }

  // list of attributes to watch for changes
  static get observedAttributes () {
    return []
  }

  attributeChangedCallback (attr, oldValue, newValue) {
  }

  // DOM updating setters for component attributes
  set recipieList (recipieList) {
    this._recipieList = recipieList
    this.renderRecipiesList()
  }

  get recipieList () {
    return this._recipieList
  }
})
