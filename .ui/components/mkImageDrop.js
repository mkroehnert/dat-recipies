// SPDX-License-Identifier: MPL-2.0
/*
  Copyright (C) 2019 The Dat Recipies Authors.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

/* globals customElements, FileReader, HTMLElement, CustomEvent */

'use strict'

import { htmlTemplate } from './mkImageDrop.htmlTemplate.js'
import { css } from './mkImageDrop.css.js'

const attributes = {
  dropzone: 'dropzone',
  dragover: 'dragover',
  noimage: 'no-image',
  imageSource: 'src'
}

function highlightDropzone (event) {
  let classList = event.target.classList
  // highlight potential drop target when the draggable element enters it
  if (classList.contains(attributes.dropzone)) {
    classList.add(attributes.dragover)
    event.preventDefault()
  }
}

function acceptDrop (self) {
  return event => {
    event.stopPropagation()
    event.preventDefault()
    if (event.target.classList.contains(attributes.dropzone)) {
      const file = event.dataTransfer.files[0]
      const reader = new FileReader()

      // only accept images
      if (file.type.match(/image.*/)) {
        reader.readAsDataURL(file)
      }

      reader.onloadend = () => {
        const imageUri = reader.result
        self.src = imageUri
      }

      return false
    }
  }
}

function emitImageUpdated (self, imageUri) {
  let event = new CustomEvent('ImageUpdated', {
    detail: {
      imageUri: imageUri
    },
    bubbles: true,
    composed: true
  })
  self.dispatchEvent(event)
}

customElements.define('mk-image-drop', class extends HTMLElement {
  constructor () {
    super()
  }

  connectedCallback () {
    const shadowRoot = this.attachShadow({ mode: 'open' })
    // append CSS first
    shadowRoot.appendChild(css())

    // append HTML second
    shadowRoot.appendChild(htmlTemplate())

    shadowRoot.addEventListener(attributes.dragover, highlightDropzone, false)
    shadowRoot.addEventListener('drop', acceptDrop(this), false)

    this.dropzone = shadowRoot.getElementById('dropzone')
    this.imgTag = null

    this.src = this.getAttribute(attributes.imageSource)
  }

  // list of attributes to watch for changes
  static get observedAttributes () {
    return [attributes.imageSource]
  }

  attributeChangedCallback (attr, oldValue, newValue) {
    const attributeName = attr.toLowerCase()
    if (attributeName === attributes.imageSource) {
      this.src = newValue !== '' ? newValue : ''
    }
  }

  // DOM updating setters for component attributes
  set src (imageDataUri) {
    if (!this.dropzone) {
      // DOM not yet connected
      return
    }
    let classList = this.dropzone.classList
    if (imageDataUri === '') {
      classList.add(attributes.noimage)
      if (this.imgTag !== null) {
        this.dropzone.removeChild(this.imgTag)
      }
      return
    }
    if (this.imgTag === null) {
      this.imgTag = document.createElement('img')
      this.dropzone.appendChild(this.imgTag)
    }

    if (this.imgTag.src !== imageDataUri) {
      this.imgTag.src = imageDataUri
      classList.remove(attributes.dragover)
      classList.remove(attributes.noimage)
      // emit event with new image URI
      emitImageUpdated(this, imageDataUri)
    }
  }

  get src () {
    if (this.imgTag === null) {
      return ''
    } else {
      return this.imgTag.src
    }
  }
})
