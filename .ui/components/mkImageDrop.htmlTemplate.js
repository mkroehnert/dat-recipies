// SPDX-License-Identifier: MPL-2.0
/*
  Copyright (C) 2019 The Dat Recipies Authors.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

'use strict'

const htmlTemplateString = `
  <template id="mk-image-drop-template">
    <div id="dropzone" class="dropzone no-image">
    </div>
  </template>
`

const htmlTemplateNode = new DOMParser()
  .parseFromString(htmlTemplateString, 'text/html')
  .querySelector('template')

export function htmlTemplate () {
  return htmlTemplateNode.content.cloneNode(true)
}
