// SPDX-License-Identifier: MPL-2.0
/*
  Copyright (C) 2019 The Dat Recipies Authors.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

'use strict'

let style = document.createElement('style')
style.textContent = `
:host {
  display: block;
  contain: content;
}

.content-grid {
  display: grid;
  grid-template-rows: auto 1fr;
  min-height: 0;
  overflow-x: hidden;
  overflow-y: auto;
}

.content-grid-header {
  grid-row: 1;
  font-size: 2em;
  font-weight: bold;
}

.content-grid-content {
  grid-row: 2;
  background: var(--recipies-foreground-color);
}


.hide {
  display: none !important;
}
`

export function css () {
  return style.cloneNode(true)
}
