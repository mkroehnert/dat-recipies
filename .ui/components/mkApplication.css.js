// SPDX-License-Identifier: MPL-2.0
/*
  Copyright (C) 2019 The Dat Recipies Authors.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

'use strict'

export const mkApplicationCss = `
:host {
  min-height: 100%;
  max-height: 100%;
  display: grid;
  align-content: stretch;
  grid-template-rows: auto 1fr auto;
  font-size: 200%;
  background: var(--recipies-background-color, green);
}

@media only screen
and (min-device-width : 320px)
and (max-device-width : 480px) {
    /* reduce font-size for mobile devices */
    :host {
	font-size: 100%;
    }
}


header {
    grid-row: 1;
    display: none;
}

main {
    grid-row: 2;
    display: grid;
    min-height: 0;
    overflow: auto;
}

footer {
    grid-row: 3;
}

#mk-application-footer::slotted(*) {
    font-size: 50%;
}
`
