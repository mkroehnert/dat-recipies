// SPDX-License-Identifier: MPL-2.0
/*
  Copyright (C) 2019 The Dat Recipies Authors.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

'use strict'

let style = document.createElement('style')
style.textContent = `
:host {
    display: block;
    contain: content;
}

div {
    width: 100%;
    height: 100%;
}

img {
    max-height: 100%;
    max-width: 100%;
    object-fit: contain;
}

.dropzone {
    display: flex;
    align-items: center;
    justify-content: center;
}

.dragover {
    background-color: var(--mk-image-drop-dragover-color) !important;
    opacity: 0.5;
}

.no-image {
    background-color: var(--mk-image-drop-background-color);
    color: var(--mk-image-drop-text-color);
    opacity: 0.5;
}

.no-image:before {
    content: "drop image here";
}
`

export function css () {
  return style.cloneNode(true)
}
