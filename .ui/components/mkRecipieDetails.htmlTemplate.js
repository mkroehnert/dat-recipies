// SPDX-License-Identifier: MPL-2.0
/*
  Copyright (C) 2019 The Dat Recipies Authors.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

'use strict'

const htmlTemplateString = `
  <template id="mk-recipie-details-template">
      <div id="recipie-details-container" class="content-grid hide">

	<div id="recipie-header" class="content-grid-header">
	  <button id="recipie-header-back">Back</button>
          <span class="spacer"></span>
          <span id="recipie-header-name">Select Recipie for Details</span>
          <span class="spacer"></span>
	  <button id="recipie-header-download">Download</button>
	</div>

	<div id="recipie-details-content" class="content-grid-content">
	  <mk-image-drop src=""></mk-image-drop>

          <h3 id="recipie-ingredients-header">Ingredients</h3>
          <ul id="recipie-ingredients"></ul>

          <h3 id="recipie-preparation-header">Preparation</h3>
          <ol id="recipie-preparation"></ol>
	</div>

      </div>
  </template>
`

const htmlTemplateNode = new DOMParser()
  .parseFromString(htmlTemplateString, 'text/html')
  .querySelector('template')

export function htmlTemplate () {
  return htmlTemplateNode.content.cloneNode(true)
}
