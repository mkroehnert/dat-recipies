// SPDX-License-Identifier: MPL-2.0
/*
  Copyright (C) 2019 The Dat Recipies Authors.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

/* globals customElements, jsPDF, HTMLElement, CustomEvent */

'use strict'

import { htmlTemplate } from './mkRecipieDetails.htmlTemplate.js'
import { css as sharedCss } from './mkRecipies.css.js'
import { css } from './mkRecipieDetails.css.js'

function getExportFilename (recipieNameTag) {
  let name = recipieNameTag.innerText
  name = name.replace(' ', '-')
  return `recipie-${name}.pdf`
}

function downloadRecipie (container, recipieNameTag) {
  return ev => {
    let pdfDocument = new jsPDF({
      orientation: 'portrait',
      unit: 'mm',
      format: 'a4'
    })

    pdfDocument.fromHTML(container.innerHTML)
    pdfDocument.save(getExportFilename(recipieNameTag))
  }
}

function removeAllChildElements (node) {
  while (node.lastChild) {
    node.removeChild(node.lastChild)
  }
}

function emitRecipieClosed (self) {
  return () => {
    let event = new CustomEvent('RecipieClosed', {
      detail: {},
      bubbles: true,
      composed: true
    })
    self.dispatchEvent(event)
  }
}

function emitRecipieImageUpdated (self, imageUri, recipieId) {
  let event = new CustomEvent('RecipieImageUpdated', {
    detail: {
      imageUri: imageUri,
      recipieId: recipieId
    },
    bubbles: true,
    composed: true
  })
  self.dispatchEvent(event)
}

customElements.define('mk-recipie-details', class extends HTMLElement {
  constructor () {
    super()
    this.currentRecipieId = null
  }

  connectedCallback () {
    this.currentRecipieId = null

    const shadowRoot = this.attachShadow({ mode: 'open' })
    // append CSS first
    shadowRoot.appendChild(sharedCss())
    shadowRoot.appendChild(css())

    // append HTML second
    shadowRoot.appendChild(htmlTemplate())

    this.getElements()
    this.setupEventListeners()
  }

  getElements () {
    this.container = this.shadowRoot.getElementById('recipie-details-container')
    this.backButton = this.shadowRoot.getElementById('recipie-header-back')
    this.recipieName = this.shadowRoot.getElementById('recipie-header-name')
    this.downloadButton = this.shadowRoot.getElementById('recipie-header-download')

    this.recipieDetails = this.shadowRoot.getElementById('recipie-details-content')
    this.imageDrop = this.shadowRoot.querySelector('mk-image-drop')
    this.ingredients = this.shadowRoot.getElementById('recipie-ingredients')
    this.preparation = this.shadowRoot.getElementById('recipie-preparation')
  }

  setupEventListeners () {
    this.backButton.addEventListener('click', emitRecipieClosed(this))
    this.downloadButton.addEventListener('click', downloadRecipie(this.recipieDetails, this.recipieName))

    this.imageDrop.addEventListener('ImageUpdated', event => {
      event.stopPropagation()
      event.preventDefault()
      emitRecipieImageUpdated(this, event.detail.imageUri, this.currentRecipieId)
    })
  }

  show (recipieDict) {
    this.renderRecipie(recipieDict)
    this.container.classList.remove('hide')
  }

  hide () {
    this.container.classList.add('hide')
  }

  renderRecipie (recipieDict) {
    if (this.currentRecipieId === recipieDict.filename) {
      // recipie has already been rendered
      return
    }

    this.currentRecipieId = recipieDict.filename
    let recipie = recipieDict.json
    this.recipieName.textContent = recipie.name

    let ingredientFragment = document.createDocumentFragment()
    recipie.ingredients.map(ingredient => {
      let li = document.createElement('li')
      li.textContent = ingredient.amount + '   ' + ingredient.type
      ingredientFragment.appendChild(li)
    })
    removeAllChildElements(this.ingredients)
    this.ingredients.appendChild(ingredientFragment)

    let preparationFragment = document.createDocumentFragment()
    recipie.preparation.map(preparationStep => {
      let li = document.createElement('li')
      li.textContent = preparationStep
      preparationFragment.appendChild(li)
    })
    removeAllChildElements(this.preparation)
    this.preparation.appendChild(preparationFragment)
    this.imageDrop.src = recipieDict.json.image
  }

  // list of attributes to watch for changes
  static get observedAttributes () {
    return []
  }

  attributeChangedCallback (attr, oldValue, newValue) {
  }
})
