// SPDX-License-Identifier: MPL-2.0
/*
  Copyright (C) 2019 The Dat Recipies Authors.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

'use strict'

export const mkApplicationHtmlTemplate = `
<template id="mk-application-template">
    <header>
        <slot id="mk-application-header" name"header">No header provided</slot>
    </header>

    <main>
        <slot id="mk-application-main" name="main">No main application provided</slot>
    </main>

    <footer>
        <slot id="mk-application-footer" name="footer">No footer provided</slot>
    </footer>
</template>
`
