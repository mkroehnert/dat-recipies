// SPDX-License-Identifier: MPL-2.0
/*
  Copyright (C) 2019 The Dat Recipies Authors.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

'use strict'

import './components/mkApplication.js'
import './components/mkImageDrop.js'
import './components/mkRecipieList.js'
import './components/mkRecipieDetails.js'

document.addEventListener('DOMContentLoaded', function () {
  if (!beaker && !beaker.hyperdrive) {
    console.log('You need to use Beaker Browser to run this application')
  }
})
