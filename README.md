# Dat Recipies

Dat Recipies allows you to store and access your recipies on the peer to peer (P2P) web using the [Dat protocol](https://datproject.org/) and [BeakerBrowser](https://beakerbrowser.com/) or soon [BunsenBrowser](https://github.com/bunsenbrowser/bunsen/blob/master/README.md).

It allows you to
* list recipies
* access recipie details
* export recipie details as PDF
* search the recipie database
* add new recipies (currently manual)
* set an image for each recipie
* delete recipies (long click/press on a recipie name for selection)

Access Dat Recipies via

* hyper URL: hyper://70f828f21841224ea3f17e3ea0f24c0d8b73870103933de66b13e5e0c3fd7bb4/


# Used Open Source Libraries and Assets

* [html2canvas.js (MIT)](https://github.com/niklasvh/html2canvas)
* [jspdf (MIT)](https://rawgit.com/MrRio/jsPDF/master/docs/index.html)
* [search SVG icon from Wikimedia Commons (public domain)](https://commons.wikimedia.org/wiki/File:Vector_search_icon.svg)

# License

The source code of Dat Recipies is available under the Mozilla Public License v2.0 (MPL-2.0).
See [LICENSE](LICENSE) for details.
